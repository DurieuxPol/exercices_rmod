"
Class part: I represent a linked list made up of links.

Responsibility part: I can create a list, add links at any position, remove links at any position. I can also get its length and any links in it, and I can test if it's empty and if it contains a given value.

Public API and Key Messages

- add:
- addEnd:
- contains:
- empty
- getAt:
- head
- isEmpty
- len
- put:at:
- removeAt:
- removeHead
- removeTail
- how to create instances:

   `MyLinkedList new` creates an empty MyLinkedList with no head (nil).
 
Internal Representation and Key Implementation Points.

	Instance Variables
		head:		<MyLink> The first link of this linked list.

"
Class {
	#name : #MyLinkedList,
	#superclass : #Object,
	#instVars : [
		'head'
	],
	#category : #MyLinkedList
}

{ #category : #adding }
MyLinkedList >> add: aLink [
	"Adds `aLink` at the start of this linked list"
	head
		ifNotNil: [ :arg | 
			| link |
			link := head.
			head := aLink.
			head next: link ].
	head ifNil: [ head := aLink ]
]

{ #category : #adding }
MyLinkedList >> add: aLink at: anInteger [
	"Adds `aLink` at the given `anInteger` position in this linked list if possible.
	Impossible if `anInteger` is either out of range for this linked list or negative"
	| linkBefore linkAfter |
	anInteger > (self len + 1)
		ifTrue: [ ^ Error new signal: 'Given index is out of range' ].
	anInteger = (self len + 1)
		ifTrue: [ ^ self addEnd: aLink ].
	anInteger <= 0
		ifTrue: [ ^ Error new signal: 'Cannot access negative or zero index' ].
	anInteger = 1
		ifTrue: [ ^ self add: aLink ].
	linkBefore := self getAt: anInteger -1.
	linkAfter := self getAt: anInteger.
	linkBefore next: aLink.
	aLink next: linkAfter
]

{ #category : #adding }
MyLinkedList >> addEnd: aLink [
	"Adds `aLink` at the end of this linked list"
	| link |
	link := head.
	head
		ifNotNil: [ :arg | 
			[ link next isNil ] whileFalse: [ link := link next ].
			link next: aLink ].
	head ifNil: [ head := aLink ]
]

{ #category : #comparing }
MyLinkedList >> contains: aVal [
	"Returns `true` if `aVal` is in this linked list, otherwise `false`"
	| link |
	self len = 0 ifTrue: [ ^ false ].
	link := head.
	[ link val = aVal or: link next isNil ]
		whileFalse: [ link := link next ].
	link val = aVal ifTrue: [ ^ true ].
	^ false
]

{ #category : #removing }
MyLinkedList >> empty [
	"Empties this linked list"
	head := nil
]

{ #category : #accessing }
MyLinkedList >> getAt: anInteger [
	"Returns the link at the `anInteger` position in this linked list, if possible.
	Impossible if `anInteger` is either out of range for this linked list or negative"
	| count link |
	anInteger > self len
		ifTrue: [ ^ Error new signal: 'Given index is out of range' ].
	anInteger = 1
		ifTrue: [ ^ head ].
	anInteger > 1
		ifFalse: [ ^ Error new signal: 'Cannot access negative or zero index' ].
	count := anInteger.
	link := head.
	[ count = 1 ]
		whileFalse: [ link := link next.
			count := count - 1 ].
	^ link
]

{ #category : #accessing }
MyLinkedList >> head [
	"Returns the first link of this linked list"
	^ head
]

{ #category : #comparing }
MyLinkedList >> isEmpty [
	"Returns `true` if this linked list is empty, otherwise `false`"
	^ head isNil
]

{ #category : #accessing }
MyLinkedList >> len [
	"Returns the length of this linked list"
	| link count |
	link := head.
	count := 1.		
	link ifNil: [ ^ 0 ].
	link ifNotNil: [ 
		[ link next isNotNil ]
			whileTrue: [ link := link next. count := count + 1 ].
		^ count ]
]

{ #category : #removing }
MyLinkedList >> removeAt: anInteger [
	"Removes the link at the `anInteger` position in this linked list, if possible.
	Impossible if `anInteger` is either out of range for this linked list, negative, or if this linked list is empty"
	| l linkBefore linkAfter |
	l := self len.
	l = 0
		ifTrue: [ ^ Error new signal: 'List is empty, cannot remove at given index' ].
	l < anInteger
		ifTrue: [ ^ Error new signal: 'Given index out of range' ].
	anInteger <= 0
		ifTrue: [ ^ Error new signal: 'Given index negative or zero' ].
	anInteger = 1
		ifTrue: [ ^ self removeHead ].
	anInteger = l
		ifTrue: [ ^ self removeTail ]
		ifFalse: [ 
			linkBefore := self getAt: (anInteger - 1).
			linkAfter := self getAt: anInteger + 1. 
			linkBefore next: linkAfter ]
	
]

{ #category : #removing }
MyLinkedList >> removeHead [
	"Removes the link at the start of this linked list, if possible.
	Impossible if this linked list is empty"
	| l |
	l := self len.
	l = 0
		ifTrue: [ ^ Error new signal: 'List is empty, cannot remove head' ].
	l = 1
		ifTrue: [ head := nil ].
	l > 1
		ifTrue: [ head := head next ]
]

{ #category : #removing }
MyLinkedList >> removeTail [
	"Removes the link at the end of this linked list, if possible.
	Impossible if this linked list is empty"
	| l link |
	l := self len.
	l = 0 
		ifTrue: [ ^ Error new signal: 'List is empty, cannot remove tail' ].
	l = 1 
		ifTrue: [ head := nil ]
		ifFalse: [ 
			link := self getAt: (l - 1).
			link unlink ]
]
