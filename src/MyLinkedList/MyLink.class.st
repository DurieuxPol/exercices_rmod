"
Class part: I represent a link.

Responsibility part: I can create a link, access its value and the next link, and I can unlink it.

Public API and Key Messages

- next
- next:
- unlink
- val
- val:
- how to create instances:

   `MyLink new` creates an empty MyLink with no value (nil) and no next (nil).
	`MyLink startingAt: aVal` creates a Mylink which value is `aVal` and with no next (nil).
 
Internal Representation and Key Implementation Points.

	Instance Variables
		val:		<Object> The value of this link, can be any type.
		next: 		<MyLink>	The link following this link.

"
Class {
	#name : #MyLink,
	#superclass : #Object,
	#instVars : [
		'val',
		'next'
	],
	#category : #MyLinkedList
}

{ #category : #initialization }
MyLink class >> startingAt: aVal [
	"Allows to create a link with `aVal` as value"
	^ self new val: aVal
]

{ #category : #accessing }
MyLink >> next [
	"Returns the link following this link, can be nil"
	^ next
]

{ #category : #setting }
MyLink >> next: aLink [
	"Sets this link's following link to `aLink`"
	next := aLink
]

{ #category : #removing }
MyLink >> unlink [
	"Unlinks this link to its following link"
	next := nil
]

{ #category : #accessing }
MyLink >> val [
	"Returns this link's value, can be nil"
	^ val
]

{ #category : #setting }
MyLink >> val: aVal [
	"Sets this link's value to `aVal`"
	val := aVal
]
