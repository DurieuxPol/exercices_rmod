"
This class contains tests for MyLink class.
"
Class {
	#name : #MyLinkTest,
	#superclass : #TestCase,
	#category : #'MyLinkedList-Tests'
}

{ #category : #tests }
MyLinkTest >> testNextIsNilAtInitialization [

	| l |
	l := MyLink new.
	self assert: l next isNil
]

{ #category : #tests }
MyLinkTest >> testNextIsSetAndRead [
		
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
 	l1 next: l2.
	self assert: l1 next equals: l2
	
]

{ #category : #tests }
MyLinkTest >> testStartingAtFive [
			
	| l |
	l := MyLink startingAt: 5.
	self assert: l val equals: 5
]

{ #category : #tests }
MyLinkTest >> testUnlink [
			
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
	l1 next: l2.
	l1 unlink.
	self assert: l1 next isNil
]

{ #category : #tests }
MyLinkTest >> testValIsSetAndRead [
			
	| l |
	l := MyLink new.
	l val: 5.
	self assert: l val equals: 5
]
