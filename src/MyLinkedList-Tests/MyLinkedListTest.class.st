"
This class contains tests for MyLinkedList class.
"
Class {
	#name : #MyLinkedListTest,
	#superclass : #TestCase,
	#instVars : [
		'list'
	],
	#category : #'MyLinkedList-Tests'
}

{ #category : #tests }
MyLinkedListTest >> setUp [
	
	super setUp.
	list := MyLinkedList new
]

{ #category : #tests }
MyLinkedListTest >> testAdd [
		
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
	list add: l1.
	list add: l2.
	self assert: list head equals: l2.
	self assert: l2 next equals: l1.
	self assert: l1 next isNil
]

{ #category : #tests }
MyLinkedListTest >> testAddAt [
	
	| l1 l2 l3 |
	l1 := MyLink new.
	l2 := MyLink new.
	l3 := MyLink new.
	list add: l1.
	list add: l2.
	list add: l3 at: 2.
	self 	assert: list head equals: l2;
			assert: (list getAt: 2) equals: l3;
			assert: (list getAt: 3) equals: l1
]

{ #category : #tests }
MyLinkedListTest >> testAddAtAddsAtEndWhenIntegerIsLen [
	
	| l1 l2 l3 |
	l1 := MyLink new.
	l2 := MyLink new.
	l3 := MyLink new.
	list add: l1.
	list add: l2.
	self 	assert: list len equals: 2; 	
			assert: ((list add: l3 at: 2) getAt: 2) equals: l3
]

{ #category : #tests }
MyLinkedListTest >> testAddAtChangesHeadWhenIntegerIsOne [
	
	| l1 l2 l3 |
	l1 := MyLink new.
	l2 := MyLink new.
	l3 := MyLink new.
	list add: l1.
	list add: l2.
	self 	assert: list head equals: l2;
			assert: (list add: l3 at: 1) head equals: l3
]

{ #category : #tests }
MyLinkedListTest >> testAddAtThrowsErrorWhenIntegerIsGreaterThanLenPlusOne [
	
	| l1 l2 l3 |
	l1 := MyLink new.
	l2 := MyLink new.
	l3 := MyLink new.
	list add: l1.
	list add: l2.
	self should: [ list add: l3 at: 4 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testAddAtThrowsErrorWhenIntegerIsNegative [
	
	| l1 |
	l1 := MyLink new.
	self should: [ list add: l1 at: -1 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testAddEnd [
			
	| l1 l2 l3 |
	l1 := MyLink new.
	l2 := MyLink new.
	l3 := MyLink new.
	list add: l1.
	list add: l2.
	list addEnd: l3.
	self assert: list head equals: l2.
	self assert: l1 next equals: l3.
	self assert: l3 next isNil
]

{ #category : #tests }
MyLinkedListTest >> testContains [
			
	| l1 |
	l1 := MyLink startingAt: 3.
	list add: l1.
	self 	assert: (list contains: 3);
			assert: (list contains: 4) not
]

{ #category : #tests }
MyLinkedListTest >> testGetAt [
			
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
 	list add: l1.
	list add: l2.
	self assert: (list getAt:2) equals: l1
]

{ #category : #tests }
MyLinkedListTest >> testGetAtThrowsErrorIfIntegerIsGreaterThanLen [
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
	list add: l1.
	list add: l2.
	self should: [ list getAt: 3 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testGetAtThrowsErrorIfIntegerIsNegativeOrZero [
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
	list add: l1.
	list add: l2.
	self 	should: [ list getAt: -3 ] raise: Error;
			should: [ list getAt: 0 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testHeadIsNilAtInitialization [
	
	self assert: list head isNil
]

{ #category : #tests }
MyLinkedListTest >> testIsEmpty [
			
	| l |
	l := MyLink new.
	self assert: list isEmpty.
	list add: l.
	list empty.
	self assert: list isEmpty
]

{ #category : #tests }
MyLinkedListTest >> testLen [
			
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
	list add: l1.
	list add: l2.
	self assert: list len equals: 2
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAt [
			
	| l1 l2 l3 |
	l1 := MyLink new.
	l2 := MyLink new.
	l3 := MyLink new.
	list add: l1.
	list add: l2.
	list add: l3.
	list removeAt: 2.
	self 	assert: list head equals: l3;
			assert: list head next equals: l1
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAtRemovesHeadWhenIntegerIsZero [
			
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
	list add: l1.
	list add: l2.
	self 	assert: list head equals: l2;
			assert: (list removeAt: 1) head equals: l1
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAtRemovesTailWhenIntegerIsLenMinusOne [
			
	| l1 l2 link |
	l1 := MyLink new.
	l2 := MyLink new.
	list add: l1.
	list add: l2.
	link := list getAt: list len.
	self 	assert: link equals: l1;
			assert: link next isNil.
	list removeAt: list len.
	link := list getAt: list len.
	self 	assert: link equals: l2;
			assert: link next isNil.
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAtThrowsErrorWhenIntegerIsGreaterThanLen [
			
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
	list add: l1.
	list add: l2.
	self should: [ list removeAt: 5 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAtThrowsErrorWhenIntegerIsNegative [
			
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
	list add: l1.
	list add: l2.
	self should: [ list removeAt: -2 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAtThrowsErrorWhenListIsEmpty [
			
	self should: [ list removeAt: 1 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testRemoveHead [
			
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
	list add: l1.
	list add: l2.
	list removeHead.
	self assert: list head equals: l1
]

{ #category : #tests }
MyLinkedListTest >> testRemoveHeadThrowsErrorIfListIsEmpty [
	
	self should: [ list removeHead ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testRemoveTail [
			
	| l1 l2 |
	l1 := MyLink new.
	l2 := MyLink new.
	list add: l1.
	list add: l2.
	list removeTail.
	self assert: list head next isNil
]

{ #category : #tests }
MyLinkedListTest >> testRemoveTailThrowsErrorIfListIsEmpty [
	
	self should: [ list removeTail ] raise: Error
]
