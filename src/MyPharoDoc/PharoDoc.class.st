"
For the Class part:  I generate documentation for a class or a package.

For the Responsibility part: I can generate documentation for a class or a package in any directory where I can write.

Public API and Key Messages

- generateDocOfClass:
- generateDocOfClass:inDirectory:
- generateDocOfPackage:
- generateDocOfPackage:inDirectory:
- writeMethods:inStream:
- writeMultipleLines:inStream:withContent:
- writeOneLine:inStream:withContent:
- writeSubclasses:inStream:
- writeVariables:inStream:
- how to create instances: All methods are class methods so no need for instance.
 
To generate documentation from a class, use `PharoDoc generateDocOfClass: <name of the class you want the documentation of> inDirectory: FileLocator (<where you want the documentation to be>)`.
To generate documentation from a package, use `PharoDoc generateDocOfPackage: (RPackageOrganizer default packageNamed: '<name of the package you want the documentation of>') inDirectory: FileLocator (<where you want the documentation to be>)`.

"
Class {
	#name : #PharoDoc,
	#superclass : #Object,
	#category : #MyPharoDoc
}

{ #category : #documentation }
PharoDoc class >> generateDocOfClass: aClass [
	"Generates documentation of `aClass` in a file 'aClass.txt' in your home folder"
	self generateDocOfClass: aClass inDirectory: FileLocator home/'Downloads'
]

{ #category : #documentation }
PharoDoc class >> generateDocOfClass: aClass inDirectory: aDirectory [
	"Generates documentation of `aClass` in a file 'aClass.txt' in `aDirectory`"
	| file |
	file := (aDirectory / aClass name, 'txt') asFileReference.
	file writeStreamDo: [ :stream |
		self writeOneLine: 'Class name: ' inStream: stream withContent: aClass name.
		self writeOneLine: 'Superclass: ' inStream: stream withContent: aClass superclass name.
		self writeSubclasses: aClass inStream: stream.
		self writeVariables: aClass inStream: stream.
		self writeMethods: aClass inStream: stream ]
]

{ #category : #documentation }
PharoDoc class >> generateDocOfPackage: aPackage [
	"Generates documentation of `aPackage` in a folder 'aPackage' in your home directory"
	self generateDocOfPackage: aPackage inDirectory: FileLocator home

]

{ #category : #documentation }
PharoDoc class >> generateDocOfPackage: aRPackage inDirectory: aDirectory [
	"Generates documentation of `aPackage` in a folder 'aPackage' in `aDirectory`"
	(aDirectory / aRPackage name) ensureCreateDirectory.
	aRPackage definedClasses
		do: [ :each | 
			self
				generateDocOfClass: each
				inDirectory: (aDirectory / aRPackage name) ]
]

{ #category : #writing }
PharoDoc class >> writeMethods: aClass inStream: aStream [
	"Writes all methods of `aClass` and their comments in `aStream`, from both class side and instance side"
	self writeOneLine: 'Class side methods:' inStream: aStream withContent: ''.
	aClass classSide methods do: [ :each |
		| content comment |
		content := each name allButFirst: (aClass greaseString) size + 9.
		comment := each comment.
		aStream 	nextPutAll: '- ';
					nextPutAll: content;
					nextPut: Character cr.
		comment isNotNil ifTrue: [ 
		aStream 	nextPutAll: comment;
					nextPut: Character cr ].
		aStream nextPut: Character cr ].
	self writeOneLine: 'Instance side methods:' inStream: aStream withContent: ''.
	aClass instanceSide methods do: [ :each |
		| content comment |
		content := each name allButFirst: (aClass greaseString) size + 3.
		comment := each comment.
		aStream 	nextPutAll: '- ';
					nextPutAll: content;
					nextPut: Character cr.
		comment isNotNil ifTrue: [ 
		aStream 	nextPutAll: comment;
					nextPut: Character cr ].
		aStream nextPut: Character cr ]
]

{ #category : #writing }
PharoDoc class >> writeMultipleLines: aString inStream: aStream withContent: aCollection [
	"Writes a part of a class documentation in the stream `aStream`, with the name `aString`, then writes each element in `aCollection` below the name, like a list"
	aStream
		nextPutAll: aString;
		nextPut: Character cr.
	aCollection
		do: [ :each | 
			aStream
				nextPutAll: '- ';
				nextPutAll: each greaseString;
				nextPut: Character cr ].
	aStream nextPut: Character cr
]

{ #category : #writing }
PharoDoc class >> writeOneLine: aString inStream: aStream withContent: content [
	"Writes a part of a class documentation in the stream `aStream`, with the name `aString` and content `content`"
	aStream
		nextPutAll: aString;
		nextPutAll: content;
		nextPut: Character cr;
		nextPut: Character cr
]

{ #category : #writing }
PharoDoc class >> writeSubclasses: aClass inStream: aStream [
	"Writes all subclasses of `aClass` in `aStream`"
	self writeMultipleLines: 'Subclasses:' inStream: aStream withContent: aClass subclasses

]

{ #category : #writing }
PharoDoc class >> writeVariables: aClass inStream: aStream [
	"Writes all variables of `aClass` in `aStream`, from both class side and instance side"
	self writeMultipleLines: 'Class variables:' inStream: aStream withContent: aClass classVariables.
	self writeMultipleLines: 'Instance variables:' inStream: aStream withContent: aClass instVarNames
]
