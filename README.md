# Exercices RMoD

Vous trouverez ici mes exercices sur les listes chaînées et la "pharodoc" en Pharo pour un stage dans l'équipe RMoD d'INRIA à Lille.

En particulier vous trouverez :

* un package `MyLinkedList` contenant les classes `MyLink` représentant le maillon de la chaîne, et `MyLinkedList` représentant la liste chaînée.

* un package `MyLinkedList-Tests` contenant les classes de tests des classes du packages `MyLinkedList`.

* un package `MyPharoDoc` contenant la classe permettant de générer la documentation d'une classe ou d'un package.

Vous trouverez également un fichier de "pharodoc" pour chaque classe à la racine du projet, tous générés grâce à la classe `PharoDoc` du package `MyPharoDoc`.

A noter que, bien que cela ne se voit pas forcément, les classes associées aux listes chaînées ont été codées en suivant le TDD.

## Génération de pharodoc

Pour générer la documentation d'une classe, utiliser:
``` pharo
    PharoDoc generateDocOfClass: <nom de la classe dont vous voulez la doc> inDirectory: FileLocator (<là où vous voulez que le fichier soit généré>)
```
, ou la version sans `inDirectory` pour générer le fichier directement dans votre dossier home (plus simple pour tester).

Pour générer la documentation d'un package, utiliser:
```pharo
    PharoDoc generateDocOfPackage: (RPackageOrganizer default packageNamed: '<nom du package dont vous voulez la doc>') inDirectory: FileLocator (<là où vous voulez que le fichier soit généré>)
```
, ou la version sans `inDirectory` pour générer le fichier directement dans votre dossier home (plus simple pour tester).